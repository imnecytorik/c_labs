 
#include <stdio.h>
#include <math.h>

int main()
{
    float c, s = 0, d = 0;
    int a, b, a1, b1;
    printf("Enter a -> ");
    scanf("%d", &a);
    printf("Enter b -> ");
    scanf("%d", &b);
    a1 = a; b1 = b;
    while (a1 > 0)
    {
        s += a1 % 10;
        a1 /= 10;
    }

    while (b1 > 0)
    {
        d += b1 % 10;
        b1 /= 10;
    }

    if (d > s) c = s/b;
    else c = d/a;

    printf("c=%f\n",c);

    return 0;
}